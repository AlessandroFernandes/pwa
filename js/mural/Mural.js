const Mural = (function(_render, Filtro){
    "use strict"
    let cartoes = chamaCartoes();

    cartoes.forEach(cartao =>
        modificarCartao(cartao))

    const render = () => _render({cartoes: cartoes, filtro: Filtro.tagsETexto});
 
    render();

    Filtro.on("filtrado", render);

    function modificarCartao(cartao){
        cartao.on("mudanca.**", salvarCartao)
        cartao.on("remocao", ()=>{
            cartoes = cartoes.slice(0)
            cartoes.splice(cartoes.indexOf(cartao),1)
            salvarCartao()
            render()
        })
    }

    function salvarCartao(){
        localStorage.setItem(usuario, JSON.stringify(
            cartoes.map(card => ({ conteudo: card.conteudo, tipo: card.tipo }))
        ))
    }
    function chamaCartoes() {
            let cartoesLocal = JSON.parse(localStorage.getItem(usuario))
    
            if(cartoesLocal){
                return JSON.parse(localStorage.getItem(usuario))
                .map(card => new Cartao(card.conteudo, card.tipo))
            }else{
                return []
            }
    }


    login.on("login", () => {
        cartoes = chamaCartoes()
        render()
    })

    login.on("logout", () => {
        cartoes = []
        render()
    })

    function adiciona(cartao){
        if(logado){
            cartoes.push(cartao)
            salvarCartao()
            cartao.on("mudanca.**", render)
            modificarCartao(cartao)
            render()
            return true
        } else {
            alert("Você não está logado")
        }
    }

    return Object.seal({
        adiciona
    })

})(Mural_render, Filtro)
